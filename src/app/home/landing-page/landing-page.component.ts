import { DataService } from './../../service/data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  subTitle = 'This is the example subtitle';
  name: string = 'You can change this text';
  jumboTitle = 'This is title coming from landing page';
  jumboSubTitle =
    'The Hindu is an English-language daily newspaper owned by The Hindu Group, headquartered in Chennai, Tamil Nadu, India. It began as a weekly in 1878 and became a daily in 1889. It is one of the Indian newspapers of record and the second most circulated English-language newspaper in India, after The Times of India';
  fullTitle!: string;
  showJumbotron!: boolean;
  constructor(
    private route: Router,
    private readonly dataSerice: DataService
  ) {}

  ngOnInit(): void {
    this.dataSerice.getMessage().subscribe((message) => {
      this.subTitle = message;
    });
  }

  submitMyData() {
    this.route.navigate(['/data']);
  }
  getFullTitle(event: any) {
    this.fullTitle = event;
  }
  showAndHideJumbotron() {
    this.showJumbotron = !this.showJumbotron;
  }
}
