import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  emailId!: string;
  password!: string;

  emailError!: string;
  passwordError!: string;
  isEmailError!: boolean;
  isPasswordError!: boolean; 

  constructor() {}

  ngOnInit(): void {}
  login() {
    this.resetFormInputs();
    if (!this.emailId) {
      this.emailError = 'Email is mandatory';
      this.isEmailError = true;
    }
    if (!this.password) {
      this.passwordError = 'Email is mandatory';
      this.isPasswordError = true;
    }

    if (!this.emailError && !this.passwordError) {
      alert(
        'submitting the form with ' + this.emailId + ' password' + this.password
      );
    }
  }

  resetFormInputs() {
    this.isEmailError = false;
    this.isPasswordError = false;
  }
}
