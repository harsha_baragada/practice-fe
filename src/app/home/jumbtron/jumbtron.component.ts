import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-jumbtron',
  templateUrl: './jumbtron.component.html',
  styleUrls: ['./jumbtron.component.css'],
})
export class JumbtronComponent implements OnInit {
  @Input() title!: string;
  @Input() subTitle!: string;
  @Output() mixedTitle = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
  triggerAlert() {
    this.mixedTitle.emit(this.title + ' ' + this.subTitle);
  }
}
