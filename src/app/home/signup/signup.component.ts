import { UserService } from './../../service/user.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [UserService],
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  matcher = new MyErrorStateMatcher();
  constructor(private readonly userService: UserService) {}

  ngOnInit(): void {
    this.signupForm = this.createSignUpForm();
  }

  createSignUpForm(): FormGroup {
    return new FormGroup({
      emailControl: new FormControl('', [
        Validators.email,
        Validators.required,
      ]),
      passwordControl: new FormControl('', Validators.required),
    });
  }

  signup() {
    if (this.signupForm.valid) {
      let user = {
        emailId: this.signupForm.controls.emailControl.value,
        password: this.signupForm.controls.passwordControl.value,
      };
      this.userService.signUpUser(user).subscribe((response) => {
        if (response == 1) {
          alert('User saved successfully!!');
        } else {
          alert('User not saved!!');
        }
      });
    } else {
      alert('Please check for some errors in the form');
    }
  }
}

export class MyErrorStateMatcher implements MyErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
