import { User } from './model/data.model';
import { DataService } from './../../service/data.service';
import {
  Component,
  DoCheck,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css'],
  providers: [],
})
export class DataComponent implements OnInit, OnChanges, DoCheck {
  users!: User[];
  constructor(private dataService: DataService) {
    console.log('constructor');
  }
  
  ngDoCheck(): void {
    throw new Error('Method not implemented.');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges');
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    console.log('ngOnInit');

    this.dataService.getData().subscribe((data) => {
      console.log(data);
      this.users = data;
    });
  }
}
