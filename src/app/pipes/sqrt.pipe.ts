import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sqrt' })
export class SquareRoot implements PipeTransform {
  transform(value: number): number {
    return Math.sqrt(value);
  }
}
