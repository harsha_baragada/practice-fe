import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DataService {
  constructor(private http: HttpClient) {}

  getData(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/posts', {
      responseType: 'json',
    });
  }

  getMessage(): Observable<any> {
    return this.http.get('http://localhost:8080/message', {
      responseType: 'text',
    });
  }
}
