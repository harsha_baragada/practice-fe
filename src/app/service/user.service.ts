import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  signUpUser(user: any): Observable<any> {
    return this.http.post('http://localhost:8080/saveUser', user);
  }
}
