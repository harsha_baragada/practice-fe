import { DataService } from './service/data.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeModule } from './home/home.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HighlightDirective } from './directives/highlight.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent, HeaderComponent, HighlightDirective],
  imports: [BrowserModule, AppRoutingModule, HomeModule, HttpClientModule, BrowserAnimationsModule],
  providers: [DataService],
  bootstrap: [AppComponent],
})
export class AppModule {}
